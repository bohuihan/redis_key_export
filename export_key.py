
import sys, getopt
import json
import redis
from tqdm import trange

def main(argv):
    host = 'localhost'
    port = '6379'
    db = 0
    match = '*'
    count = 1000
    outputfile = 'output.csv'
    outputformat = 'csv'

    try:
        opts, args = getopt.getopt(argv,"hi:p:d:m:c:o:f:",["ihost=","port=","db=","match=","count=","outputfile=", "format="])
    except getopt.GetoptError:
        print ('export_key.py -i <host> -p <port> -d <db_number> -m <match_pattern> -c <count_per_page> -o <outputfile>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print ('export_key.py -i <host> -p <port> -d <db_number> -m <match_pattern> -c <count_per_page> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ihost"):
            host = arg
        elif opt in ("-p", "--port"):
            port = arg
        elif opt in ("-d", "--db"):
            db = arg
        elif opt in ("-m", "--match"):
            match = arg
        elif opt in ("-c", "--count"):
            count = int(arg)
        elif opt in ("-o", "--outputfile"):
            outputfile = arg
        elif opt in ("-f", "--format"):
            outputformat = arg
            if outputformat not in ('csv', 'json'):
                print("Only 'csv' and 'json' format are supported")
                exit()

    r_client = redis.StrictRedis(host=host, port=port, db=db)
    keyspace_info = r_client.info('keyspace')

    keys_count = keyspace_info.get('db{}'.format(db), {}).get('keys')

    if keys_count is None:
        print ("There is no keys in db{}.".format(db))
        exit()

    keys_count = int(keys_count)

    if outputformat == 'csv':
        with open(outputfile, 'w') as f, trange(keys_count) as progress_bar:
            for key in r_client.scan_iter(match, count):
                f.write(key+"\n")
                progress_bar.update()

    elif outputformat == 'json':
        output = {}
        with trange(keys_count) as progress_bar:
            for key in r_client.scan_iter(match, count):
                parts = key.split(":")
                cur_item = output
                for p in parts:
                    cur_item = cur_item.setdefault(p, {})
                progress_bar.update()

        with open(outputfile, 'w') as f:
            f.write(json.dumps(output, sort_keys=True, indent=4))

if __name__ == "__main__":
   main(sys.argv[1:])
